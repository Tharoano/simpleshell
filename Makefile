DEBUG=y

CC=gcc
#CFLAGS= -Werror -Wall -DDEBUG -g
LDFLAGS= 

ifeq ($(DEBUG), y)
	CFLAGS += -Werror -Wall -DDEBUG -g
else
	CFLAGS += -O2
endif



all: bin/shell

bin/shell: obj/shell.o obj/parse.o
	gcc $(CFLAGS) $^ -o $@

obj/%.o: src/%.c
	gcc -c $(CFLAGS) $< -o $@

clean:
	$(RM) *~ src/*~ src/#* obj/*.o bin/* 

.PHONY: all clean dist

